#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2020 Jérôme Carretero <cJ@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT
# Convenience stuff for listening and connecting

import socket
import os
import json
import logging
import urllib.parse
from http.client import HTTPConnection
from typing import *

logger = logging.getLogger(__name__)


def http_post(url, path, data):
    conn, _ = HTTPConnection_from_url(url)

    json_str = json.dumps(data)
    if data is not None:
        conn.request("POST", path, json_str.encode("utf-8"))
    else:
        conn.request("POST", path)

    resp = conn.getresponse()

    try:
        data = json.loads(resp.read().decode("utf-8"))
    except json.decoder.JSONDecodeError:
        data = resp.read().decode("utf-8")

    conn.close()

    return resp.status, data


def http_get(url, path):
    conn, _ = HTTPConnection_from_url(url)

    conn.request("GET", path)
    resp = conn.getresponse()

    try:
        data = json.loads(resp.read().decode("utf-8"))
    except json.decoder.JSONDecodeError:
        data = resp.read().decode("utf-8")

    conn.close()

    return resp.status, data


def listener_from_url(listen_url, server_klass, handler_klass):
	"""
	Create a socket server from url spec
	"""

	parseresult = urllib.parse.urlparse(listen_url)
	qs = urllib.parse.parse_qs(parseresult.query)
	if parseresult.scheme == "unix":
		sock_file = parseresult.netloc + parseresult.path
		logger.info("Listening on UNIX: %s", sock_file)
		if os.path.exists(sock_file):
			os.unlink(sock_file)
		sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_PASSCRED, 1)
		sock.bind(sock_file)
		sock.listen(15)
		os.chmod(sock_file, 0o666)
		server = server_klass(sock_file, handler_klass, bind_and_activate=False)
		server.socket = sock

	elif parseresult.scheme == "tcp":
		netloc = parseresult.netloc
		if ":" in netloc:
			host, port = netloc.split(":")
			port = int(port)
		else:
			host = netloc
			port = 8080
		logger.info("Listening on TCP: %s:%d", host, port)
		server = server_klass((host, port), handler_klass)

	elif parseresult.scheme == "fd":
		"""
		Create server on socket from open file descriptor.

		Syntax, eg. fd://3

		Typical use case for systemd socket-activated services,
		 SD_LISTEN_FDS_START, see man sd_listen_fds
		"""

		server = server_klass(None, handler_klass, bind_and_activate=False)
		fd = int(parseresult.netloc)
		sock = socket.fromfd(
		 fd=fd,
		 family=server.address_family, # doesn't matter? TODO SO_PASSCRED
		 type=server.socket_type,
		)
		server.socket = sock

	else:
		raise NotImplementedError(listen_url)

	return server


def HTTPConnection_from_url(connect_url) -> Tuple[HTTPConnection,str]:
	"""
	:return: HTTPConnection and potentially modified url
	"""

	parseresult = urllib.parse.urlparse(connect_url)
	qs = urllib.parse.parse_qs(parseresult.query)
	if parseresult.scheme == "unix":
		path = parseresult.path
		sock_file = parseresult.netloc + path
		path = ""
		while True:
			logger.debug("Trying socket file %s, path %s", sock_file, path)

			#statbuf = os.stat(sock_file)
			if not os.path.exists(sock_file):
				# Try parent
				sock_file, a = os.path.split(sock_file)
				path = "/{}{}".format(a, path)
			else:
				break

			if sock_file == "":
				raise ValueError("Couldn't find socket file for {}".format(connect_url))

		url = urllib.parse.urlunparse(
		 ("http", "localhost", path, parseresult.params, parseresult.query, parseresult.fragment)
		)

		logger.info("Connecting to UNIX: %s for %s", sock_file, url)

		conn = HTTPConnection("localhost")
		conn.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
		conn.sock.connect(sock_file)
	elif parseresult.scheme == "http":
		conn = HTTPConnection(parseresult.netloc)
		url = connect_url
	else:
		raise NotImplementedError(parseresult)

	return conn, url
