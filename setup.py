from setuptools import setup

setup(
 name="konvini",
 version="0.1",
 python_requires=">=3.7",
 description="Small convenience stuff",
 author="ExMakhina",
 license="MIT",
 keywords="",
 install_requires=[],
 packages=[
  "exmakhina.konvini",
 ],
)
